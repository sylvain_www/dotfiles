conf:
  always_on: 'on'
  audio:
    card: '0'
    sink: '0'
  autolock: 'on'
  device:
  - sdb1
  - mmcblk0p1
  dwm:
    border: '1'
    font: DejaVu Sans Mono:pixelsize=16:antialias=true:hinting=true:style=bold
    gap: '6'
  keyboard:
    fr: AT Translated Set 2 keyboard
    us: TypeMatrix.com USB Keyboard.*keyboard
  network:
    renderer: NetworkManager
    lan: enp0s31f6
    wifi: wlp1s0
  output: eDP-1
  outputs:
  - crtc: '0'
    height: '1080'
    name: eDP-1
    width: '1920'
  - crtc: '1'
    height: '1080'
    name: DP-1-1
    position: --right-of eDP-1
    width: '1920'
  - crtc: '2'
    height: '1080'
    name: DP-1-2
    position: --right-of DP-1-1
    width: '1920'
  - height: '1080'
    name: HDMI-1
    position: --above eDP-1
    width: '1920'
  default_python_version: 3.11.3
  statnot:
    columns: '114'
    counters:
    - email
    - battery
    - volume
    interval: '1'
    margin: '4'
    scroll_speed: '0.15'
    timeout: '6'
  terminal:
    font: xft:Inconsolata:pixelsize=18:antialias=true:hinting=true:style=medium
    italicfont: xft:inconsolata:bold:underline:autohint=true
  touchpad:
    name: lpsPS/2 ALPS DualPoint TouchPad
