conf:
  always_on: 'off'
  audio:
    card: '0'
    sink: '0'
  autolock: 'on'
  device:
  - sdb1
  - mmcblk0p1
  dwm:
    border: '2'
    font: Dejavu Sans Mono:medium:pixelsize=14:antialias=true:hinting=true
    gap: '6'
  keyboard:
    fr: AT Translated Set 2 keyboard
    us: TypeMatrix.com USB Keyboard.*keyboard
  network:
    renderer: NetworkManager
    lan: TODO
    wifi: TODO
  output: LVDS1
  outputs:
  - height: '600'
    name: LVDS1
    width: '1024'
  default_python_version: 3.11.3
  statnot:
    columns: '54'
    counters:
    - email
    - battery
    - volume
    interval: '1'
    margin: '2'
    scroll_speed: '0.15'
    timeout: '6'
  terminal:
    font: xft:Inconsolata:pixelsize=16:antialias=true:hinting=true:style=medium
    italicfont: xft:inconsolata:bold:underline:autohint=true
  touchpad:
    name: SynPS/2 Synaptics TouchPad
