conf:
  always_on: 'off'
  audio:
    card: '0'
    sink: '0'
  autolock: 'off'
  device:
  - sdb1
  dwm:
    border: '1'
    font: DejaVu Sans Mono:pixelsize=16:antialias=true:hinting=true:style=bold
    gap: '6'
  keyboard:
    fr: ''
    us: TypeMatrix.com USB Keyboard.*keyboard
  network:
    renderer: networkctl
    lan: enp2s0
    fixed_ip: 192.168.0.100
    gateway: 192.168.0.1
  output: DP-2
  outputs:
  - height: '1200'
    name: DP-2
    width: '1920'
  - height: '1080'
    name: HDMI-1
    position: --left-of DP-2
    width: '1920'
  default_python_version: 3.11.3
  statnot:
    columns: '120'
    counters:
    - email
    - volume
    interval: '1'
    margin: '4'
    scroll_speed: '0.15'
    timeout: '6'
  terminal:
    font: xft:Inconsolata:pixelsize=18:antialias=true:hinting=true:style=medium
    italicfont: xft:inconsolata:bold:underline:autohint=true
