conf:
  always_on: 'on'
  audio:
    card: '0'
    sink: '2'
  autolock: 'on'
  device:
  - sda1
  dwm:
    border: '1'
    font: DejaVu Sans Mono:pixelsize=16:antialias=true:hinting=true:style=bold
    gap: '6'
  keyboard:
    fr: ''
    us: ''
  network:
    renderer: NetworkManager
    lan: enxcc483a7b0e4b
    wifi: wlo1
  output: eDP-1
  outputs:
  - crtc: '0'
    height: '1080'
    name: eDP-1
    width: '1920'
  - crtc: '1'
    height: '1080'
    name: DP-1-3
    position: --right-of eDP-1
    width: '1920'
  - height: '1080'
    name: HDMI-1
    position: --above eDP-1
    width: '1920'
  default_python_version: 3.11.3
  statnot:
    columns: '116'
    counters:
    - email
    - battery
    - volume
    interval: '1'
    margin: '4'
    scroll_speed: '0.15'
    timeout: '6'
  terminal:
    font: xft:Inconsolata:pixelsize=18:antialias=true:hinting=true:style=medium
    italicfont: xft:inconsolata:bold:underline:autohint=true
  touchpad:
    name: DELL08E1:00 06CB:CD97 Touchpad
