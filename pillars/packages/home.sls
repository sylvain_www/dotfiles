packages:
  home:
  - gramps
  - grisbi
  - mp3info
  - picard
  - rsync
  - rtorrent
not_packages:
  home:
  - cups
pip_packages:
  home: []
flatpak:
  home:
  - name: org.videolan.VLC
    exe: vlc
