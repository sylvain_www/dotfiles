packages:
  common:
  - build-essential
  - cups
  - curl
  - default-jdk
  - gdb
  - gimp
  - gparted
  - gv
  - htop
  - inotify-tools
  - iotop
  - jq
  - language-pack-fr
  - less
  - links
  - lxappearance
  - mplayer
  - pcmanfm
  - pdftk
  - pinta
  - tree
  - unclutter
  - unzip
  - vim-gtk3
not_packages:
  home:
  - notify-osd
pip_packages:
  common:
  - black
  - ipdb
  - ipython
  - eg
  - yq
flatpak:
  common:
  - name: org.gnome.meld
    exe: meld
  - name: org.inkscape.Inkscape
    exe: inkscape
