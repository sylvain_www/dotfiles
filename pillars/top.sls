base:
  '*':
  - git
  - radio
  - secrets
  - packages.common
  akasa:
  - conf.akasa
  - menu.home
  - packages.home
  e7470:
  - conf.e7470
  - menu.scality
  - packages.scality
  eeepc:
  - conf.eeepc
  - menu.home
  - packages.home
  l7400:
  - conf.l7400
  - menu.vianova
  - packages.vianova
