radio:
- name: frisky
  url: http://stream2.friskyradio.com/frisky_mp3_hi
- name: frisky chill
  url: http://chill.friskyradio.com/friskychill_mp3_high
- name: frisky deep
  url: http://deep.friskyradio.com/friskydeep_mp3_high
- name: radio en construction
  url: https://str0.creacast.com/rec
- name: radio meuh
  url: http://radiomeuh.ice.infomaniak.ch/radiomeuh-128.mp3
- name: france inter
  url: http://direct.franceinter.fr/live/franceinter-midfi.mp3
- name: france info
  url: http://direct.franceinfo.fr/live/franceinfo-midfi.mp3
- name: france culture
  url: http://direct.franceculture.fr/live/franceculture-midfi.mp3
- name: france musique
  url: http://direct.francemusique.fr/live/francemusique-midfi.mp3
- name: fip
  url: http://direct.fipradio.fr/live/fip-midfi.mp3
- name: fip rock
  url: http://direct.fipradio.fr/live/fip-webradio1.mp3
- name: fip electro
  url: http://direct.fipradio.fr/live/fip-webradio8.mp3
- name: fip groove
  url: http://direct.fipradio.fr/live/fip-webradio3.mp3
- name: le mouv
  url: http://direct.mouv.fr/live/mouv-midfi.mp3
- name: eilo progressive
  url: http://stream.eilo.org:8000/progressive
- name: eilo house
  url: http://stream.eilo.org:8000/house
- name: eilo psychedelic
  url: http://stream.eilo.org:8000/psychedelic
- name: eilo trance
  url: http://stream.eilo.org:8000/trance
- name: ancient fm
  url: https://mediaserv73.live-streams.nl:18058/stream
