{{ grains.homedir }}/src/bitbucket/sylvain_www:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true

{{ grains.homedir }}/src/github:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true

{{ grains.homedir }}/src/gitlab:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true

git:
  user_include:
  - condition: gitdir:~/src/bitbucket/**
    include: ~/.gitconfig-home-bitbucket
  - condition: gitdir:~/src/bitbucket/scality/**
    include: ~/.gitconfig-scality-bitbucket
  - condition: gitdir:~/src/github/scality/**
    include: ~/.gitconfig-scality-github
  - condition: gitdir:~/src/gitlab/vianova/**
    include: ~/.gitconfig-vianova-gitlab
