android packages:
  pkg.installed:
  - pkgs:
    - scrcpy
    - nmap

{{ grains.bindir }}/android:
  file.managed:
  - source: salt://android/files/android
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja
