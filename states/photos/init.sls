include:
- python
- system

{{ grains.bindir }}/photos:
  file.managed:
  - source: salt://photos/files/photos
  - template: jinja
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700

{{ grains.homedir }}/.photos:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 755

{{ grains.homedir }}/.photos/client_secrets.json:
  file.managed:
  - source: salt://photos/files/client_secrets.json
  - template: jinja
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - mode: 400

{{ grains.homedir }}/.photos/config:
  file.managed:
  - source: salt://photos/files/config
  - template: jinja
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 644

{{ grains.homedir }}/.photos/docker-compose.yml:
  file.managed:
  - source: salt://photos/files/docker-compose.yml
  - template: jinja
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 644

{{ grains.homedir }}/.photos/photoprism/config/settings.yml:
  file.managed:
  - source: salt://photos/files/settings.yml
  - template: jinja
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 644

photos archiver checked-out:
  git.cloned:
  - name: git@home.bitbucket.org:sylvain_www/photos_archiver
  - user: {{ grains.user }}
  - target: {{ grains.homedir }}/src/bitbucket/sylvain_www/photos_archiver
  - require:
    - sls: git

photos archiver virtualenv:
  cmd.run:
  - name: >
      pyenv virtualenvs --bare | grep -q "^photos$"
      || pyenv virtualenv {{ pillar.conf.default_python_version}} photos
  - runas: {{ grains.user }}
  - env:
    - PATH: /usr/bin:{{ grains.homedir }}/.pyenv/bin
    - PYENV_ROOT: {{ grains.homedir }}/.pyenv
  - require:
    - sls: python

photos archiver packages:
  pip.installed:
  - bin_env: {{ grains.homedir }}/.pyenv/versions/photos
  - user: {{ grains.user }}
  - requirements: {{ grains.homedir }}/src/bitbucket/sylvain_www/photos_archiver/requirements.txt
  - require:
    - git: photos archiver checked-out
    - cmd: photos archiver virtualenv

{{ grains.homedir }}/.photos/.python-version:
  file.managed:
  - source: salt://photos/files/python-version
  - template: jinja
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 400
  - require:
    - git: photos archiver checked-out

{{ grains.homedir }}/.config/systemd/user/photoprism.service:
  file.managed:
  - source: salt://photos/files/photoprism.service
  - makedirs: true
  - mode: 755
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.homedir }}/.config/systemd/user/default.target.wants/photoprism.service:
  file.symlink:
  - target: {{ grains.homedir }}/.config/systemd/user/photoprism.service

{{ grains.homedir }}/.photos/dockerfile_patch:
  file.managed:
  - source: salt://photos/files/dockerfile_patch
  - template: jinja
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 400

photoprism patched image:
  docker_image.present:
    - name: photoprism
    - build: {{ grains.homedir }}/.photos
    - tag: patched
    - dockerfile: dockerfile_patch
