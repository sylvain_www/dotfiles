include:
  - python

system groups for user:
  user.present:
  - name: {{ grains.user }}
  - remove_groups: false
  - groups:
    - adm
    - audio
    - dip
    - sudo

system packages:
  pkg.installed:
  - pkgs:
    {%- for plist in pillar.packages.values() %}
    {%- for package in plist %}
    - {{ package }}
    {%- endfor %}
    {%- endfor %}
  - require:
    - sls: python

remove unwanted desktop packages:
  pkg.removed:
  - pkgs:
    {%- for plist in pillar.not_packages.values() %}
    {%- for package in plist %}
    - {{ package }}
    {%- endfor %}
    {%- endfor %}

python pip packages:
  pip.installed:
  - pkgs:
    {%- for plist in pillar.pip_packages.values() %}
    {%- for package in plist %}
    - {{ package }}
    {%- endfor %}
    {%- endfor %}
  - require:
    - sls: python

set up local timezone:
  timezone.system:
  - name: Europe/Paris

us_locale:
  locale.present:
  - name: en_US.UTF-8

fr_locale:
  locale.present:
  - name: fr_FR.UTF-8

default_locale:
  locale.system:
  - name: en_US.UTF-8
  - require:
    - locale: us_locale

crontab path configured:
  pkg.installed:
  - pkgs:
    - cron
  cron.env_present:
  - name: PATH
  - value: {{ grains.bindir }}:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
  - user: {{ grains.user }}

crontab configured:
  cron.env_present:
  - name: SHELL
  - value: /usr/bin/zsh
  - user: {{ grains.user }}
  - require:
    - cron: crontab path configured
