chrome repository:
  pkgrepo.managed:
  - humanname: Google Chrome
  - name: deb http://dl.google.com/linux/chrome/deb/ stable main
  - dist: stable
  - file: /etc/apt/sources.list.d/google-chrome.list
  - gpgcheck: 1
  - key_url: https://dl-ssl.google.com/linux/linux_signing_key.pub

chrome packages:
  pkg.installed:
  - pkgs:
    - google-chrome-stable
  - require:
    - pkgrepo: chrome repository
