grisbi packages:
  pkg.installed:
  - pkgs:
    - grisbi

{{ grains.homedir }}/.config/grisbi/grisbi.conf:
  file.managed:
  - source: salt://grisbi/files/grisbi.conf
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - mode: 664
  - makedirs: true
  - template: jinja
