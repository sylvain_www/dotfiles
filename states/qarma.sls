include:
- builddir
- git

qarma packages:
  pkg.installed:
  - pkgs:
    - build-essential
    - qt5-qmake
    - qtchooser
    - qtbase5-dev
    - libqt5svg5-dev
    - libpng-dev
    - qtbase5-dev-tools
    - qtbase5-private-dev
    - libqt5x11extras5-dev

qarma checked-out:
  git.latest:
  - name: https://github.com/luebking/qarma.git
  - user: {{ grains.user }}
  - target: {{ grains.builddir }}/qarma
  - force_checkout: True
  - force_fetch: True
  - force_reset: True
  - require:
    - sls: git

qarma compiled:
  cmd.run:
  - name: >
      cd {{ grains.builddir }}/qarma &&
      qmake &&
      make -j4
  - require:
    - git: qarma checked-out
    - pkg: qarma packages

{{ grains.bindir }}/qarma:
  file.managed:
  - source: {{ grains.builddir }}/qarma/qarma
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - mode: 700
  - makedirs: True
  - encoding_errors: ignore
  - require:
    - cmd: qarma compiled
