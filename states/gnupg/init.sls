gnupg packages:
  pkg.installed:
  - pkgs:
    - gnupg

{{ grains.homedir }}/.gnupg:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700

{{ grains.homedir }}/.gnupg/pubring.kbx:
  file.managed:
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - mode: 600

{{ grains.homedir }}/.gnupg/gpg.conf:
  file.managed:
  - source: salt://gnupg/files/gpg.conf
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - mode: 600
  - makedirs: true
  - template: jinja
