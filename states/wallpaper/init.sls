{% set dir = grains.homedir + '/.wallpaper' %}

wallpaper packages:
  pkg.installed:
  - pkgs:
    - feh
    - imagemagick

{{ dir }}:
  file.recurse:
  - source: salt://wallpaper/files/src
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - unless:
    - '[ -f {{ dir }}/.converted ]'

{% set output = (pillar.conf.outputs|selectattr("name", "equalto", pillar.conf.output)|list)[0] %}

convert wallpapers to local resolution:
  cmd.run:
  - name: >
      for file in $(ls); do convert -resize
      {{ output.width }}x{{ output.height }}!
      ${file} ${file};
      done
  - cwd: {{ dir }}
  - unless:
    - '[ -f {{ dir }}/.converted ]'
  - require:
    - pkg: wallpaper packages
    - file: {{ dir }}

  file.managed:
  - name: {{ dir }}/.converted
  - replace: False

{{ grains.bindir }}/wallpaper:
  file.managed:
  - source: salt://wallpaper/files/wallpaper
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
