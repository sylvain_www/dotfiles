include:
- contacts

/home/private:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}

{{ grains.homedir }}/private:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}

fstab entry:
  mount.mounted:
  - name: {{ grains.homedir }}/private
  - device: /home/private
  - mount: false
  - mkmnt: true
  - fstype: ecryptfs
  - opts:
    - noauto
    - user
    - ecryptfs_cipher=aes
    - ecryptfs_key_bytes=16
    - ecryptfs_sig={{ pillar.private.signature }}
    - ecryptfs_fnek_sig={{ pillar.private.signature }}

{{ grains.bindir }}/priv:
  file.managed:
  - source: salt://private/files/priv
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja

{{ grains.bindir }}/priv_ask:
  file.managed:
  - source: salt://private/files/priv_ask
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja

{{ grains.bindir }}/upload_phone_private:
  file.managed:
  - source: salt://private/files/upload_phone_private
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja

{{ grains.bindir }}/buddies:
  file.managed:
  - source: salt://private/files/buddies
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja

buddies virtualenv:
  cmd.run:
  - name: >
      pyenv virtualenvs --bare | grep -q "^contacts$"
      || pyenv virtualenv {{ pillar.conf.default_python_version}} contacts
  - runas: {{ grains.user }}
  - env:
    - PATH: /usr/bin:{{ grains.homedir }}/.pyenv/bin
    - PYENV_ROOT: {{ grains.homedir }}/.pyenv
  - require:
    - sls: python

buddies checked-out:
  git.cloned:
  - name: git@home.bitbucket.org:sylvain_www/buddies
  - user: {{ grains.user }}
  - target: {{ grains.homedir }}/src/bitbucket/sylvain_www/buddies
  - require:
    - sls: git

buddies packages:
  pip.installed:
  - bin_env: {{ grains.homedir }}/.pyenv/versions/contacts
  - user: {{ grains.user }}
  - requirements: {{ grains.homedir }}/src/bitbucket/sylvain_www/buddies/requirements.txt
  - require:
    - git: buddies checked-out
    - cmd: buddies virtualenv

{{ grains.homedir }}/.config/systemd/user/buddies.service:
  file.managed:
  - source: salt://private/files/buddies.service
  - makedirs: true
  - mode: 755
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.homedir }}/.config/systemd/user/default.target.wants/buddies.service:
  file.symlink:
  - target: {{ grains.homedir }}/.config/systemd/user/buddies.service
