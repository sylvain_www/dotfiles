git packages:
  pkg.installed:
  - pkgs:
    - git
    - tig

{{ grains.homedir }}/.gitconfig:
  file.managed:
  - source: salt://git/files/gitconfig
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.homedir }}/.tigrc:
  file.managed:
  - source: salt://git/files/tigrc
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{% for name, user in pillar.git.users.items() %}
{{ grains.homedir }}/.gitconfig-{{ name }}:
  file.managed:
  - source: salt://git/files/gitconfig-user
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
  - defaults:
     name: {{ user.name }}
     email: {{ user.email }}
{% endfor %}

{{ grains.homedir }}/src/github:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700

{{ grains.homedir }}/src/bitbucket:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700

{{ grains.homedir }}/src/gitlab:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
