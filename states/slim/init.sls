include:
- slim_theme

slim packages:
  pkg.installed:
  - pkgs:
    - slim

/etc/slim.conf:
  file.managed:
  - source: salt://slim/files/slim.conf
  - user: root
  - group: root
  - template: jinja
