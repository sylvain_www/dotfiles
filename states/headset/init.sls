headset packages:
  pkg.installed:
  - pkgs:
    - paprefs
    - pavucontrol
    - pulseaudio-module-bluetooth

additional groups for bluetooth user:
  user.present:
  - name: {{ grains.user }}
  - remove_groups: false
  - groups:
    - audio
    - bluetooth

{{ grains.bindir }}/headset:
  file.managed:
  - source: salt://headset/files/headset
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja
