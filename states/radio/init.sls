include:
- music

{{ grains.homedir }}/music/radios:
  file.directory:
  - makedirs: true
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - mode: 750

{% for radio in pillar.radio %}
{{ grains.homedir }}/music/radios/{{ loop.index }}.pls:
  file.managed:
  - source: salt://radio/files/pls
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
  - defaults:
      name: {{ radio.name }}
      url: {{ radio.url }}
  - require:
    - file: {{ grains.homedir }}/music/radios
{% endfor %}

{{ grains.bindir }}/radio:
  file.managed:
  - source: salt://radio/files/radio
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
