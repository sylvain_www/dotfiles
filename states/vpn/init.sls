{% if "vpn" in pillar and pillar.vpn %}
vpn packages:
  pkg.installed:
  - pkgs:
    - openvpn
    - resolvconf

{% for provider in pillar.vpn %}
{% for file in pillar.vpn[provider] %}
{{ grains.homedir }}/.vpn/{{ provider }}/{{ file }}:
  file.managed:
  - contents_pillar: vpn:{{ provider }}:{{ file }}
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 600
  - template: jinja
{% endfor %}
{% endfor %}

{{ grains.bindir }}/vpn:
  file.managed:
  - source: salt://vpn/files/vpn
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
{% endif %}
