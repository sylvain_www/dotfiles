include:
- gdrive

{{ grains.homedir }}/cloud/gdrive_scality:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - require:
    - file: {{ grains.bindir }}/drive
