include:
- gdrive

{{ grains.homedir }}/cloud/gdrive:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
