include:
- builddir
- font
- git
- system

dwm packages:
  pkg.installed:
  - pkgs:
    - libxcursor-dev
    - libxft-dev
    - libxinerama-dev

dwm checked-out:
  git.latest:
  - name: git@home.bitbucket.org:sylvain_www/dwm
  - user: {{ grains.user }}
  - target: {{ grains.builddir }}/dwm
  - force_checkout: True
  - force_fetch: True
  - force_reset: True
  - require:
    - sls: git

dwm compiled:
  cmd.run:
  - name: >
      make clean -C {{ grains.builddir }}/dwm &&
      make -C {{ grains.builddir }}/dwm
  - env:
      BORDERPX: '{{ pillar.conf.dwm.border }}'
      FONT: '{{ pillar.conf.dwm.font }}'
      GAPPX: '{{ pillar.conf.dwm.gap }}'
      COUNTERS: '{{ pillar.conf.statnot.counters|length }}'
  - require:
    - git: dwm checked-out
    - pkg: dwm packages
    - pkg: system packages

{{ grains.bindir }}/dwm:
  file.managed:
  - source: {{ grains.builddir }}/dwm/dwm
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - mode: 700
  - makedirs: True
  - encoding_errors: ignore
  - require:
    - cmd: dwm compiled
    - pkg: font packages

dwm user repo checked-out:
  git.cloned:
  - name: git@home.bitbucket.org:sylvain_www/dwm
  - user: {{ grains.user }}
  - target: {{ grains.homedir }}/src/bitbucket/sylvain_www/dwm
  - require:
    - sls: git
