vifm packages:
  pkg.installed:
  - pkgs:
    - vifm
    - xclip

{{ grains.homedir }}/.vifm/vifmrc:
  file.managed:
  - source: salt://vifm/files/vifmrc
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - template: jinja

{{ grains.homedir }}/.vifm/colors/colors.vifm:
  file.managed:
  - source: salt://vifm/files/colors
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - template: jinja
