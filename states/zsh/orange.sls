{{ grains.homedir }}/.zshrc_orange:
  file.managed:
  - source: salt://zsh/files/zshrc_orange
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
