{{ grains.homedir }}/.zshrc_scality:
  file.managed:
  - source: salt://zsh/files/zshrc_scality
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
