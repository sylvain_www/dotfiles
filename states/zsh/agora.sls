{{ grains.homedir }}/.zshrc_agora:
  file.managed:
  - source: salt://zsh/files/zshrc_agora
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
