{{ grains.homedir }}/.zshrc_vianova:
  file.managed:
  - source: salt://zsh/files/zshrc_vianova
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
