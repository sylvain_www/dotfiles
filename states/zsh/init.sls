zsh packages:
  pkg.installed:
  - pkgs:
    - colordiff
    - diffutils
    - zsh

{{ grains.homedir }}/.zshrc:
  file.managed:
  - source: salt://zsh/files/zshrc
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.homedir }}/.dircolors:
  file.managed:
  - source: salt://zsh/files/dircolors
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.homedir }}/.colordiffrc:
  file.managed:
  - source: salt://zsh/files/colordiffrc
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
