{{ grains.homedir }}/.zshrc_android:
  file.managed:
  - source: salt://zsh/files/zshrc_android
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
