{{ grains.homedir }}/.icons/FlatbedCursors.White.Small:
  file.recurse:
  - source: salt://cursor/files/FlatbedCursors.White.Small
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true

{{ grains.homedir }}/.icons/default:
  file.symlink:
  - target: {{ grains.homedir }}/.icons/FlatbedCursors.White.Small
