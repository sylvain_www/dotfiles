screenshot packages:
  pkg.installed:
  - pkgs:
    - scrot

{{ grains.homedir }}/sandbox/new_screenshots:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}

{{ grains.bindir }}/screenshot:
  file.managed:
  - source: salt://screenshot/files/screenshot
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja
