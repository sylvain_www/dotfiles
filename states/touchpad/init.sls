{{ grains.bindir }}/touchpad:
  file.managed:
  - source: salt://touchpad/files/touchpad
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja
