include:
- python
- system

{{ grains.bindir }}/recos:
  file.managed:
  - source: salt://recos/files/recos
  - template: jinja
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700

recos checked-out:
  git.cloned:
  - name: git@home.bitbucket.org:sylvain_www/recos
  - user: {{ grains.user }}
  - target: {{ grains.homedir }}/src/bitbucket/sylvain_www/recos
  - require:
    - sls: git

recos virtualenv:
  cmd.run:
  - name: >
      pyenv virtualenvs --bare | grep -q "^recos$"
      || pyenv virtualenv {{ pillar.conf.default_python_version}} recos
  - runas: {{ grains.user }}
  - env:
    - PATH: /usr/bin:{{ grains.homedir }}/.pyenv/bin
    - PYENV_ROOT: {{ grains.homedir }}/.pyenv
  - require:
    - sls: python

recos packages:
  pip.installed:
  - bin_env: {{ grains.homedir }}/.pyenv/versions/recos
  - user: {{ grains.user }}
  - requirements: {{ grains.homedir }}/src/bitbucket/sylvain_www/recos/requirements.txt
  - require:
    - git: recos checked-out
    - cmd: recos virtualenv

{{ grains.homedir }}/src/bitbucket/sylvain_www/recos/.python-version:
  file.managed:
  - source: salt://recos/files/python-version
  - template: jinja
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 400
  - require:
    - git: recos checked-out
