network packages:
  pkg.installed:
  - pkgs:
    - network-manager

{{ grains.bindir }}/network:
  file.managed:
  - source: salt://network/files/network
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

/etc/netplan/00-installer-config.yaml:
  file.managed:
  - source: salt://network/files/00-installer-config.yaml
  - makedirs: false
  - mode: 644
  - user: root
  - group: root
  - template: jinja

/etc/NetworkManager/NetworkManager.conf:
  file.managed:
  - source: salt://network/files/NetworkManager.conf
  - makedirs: false
  - mode: 644
  - user: root
  - group: root
  - template: jinja

/etc/NetworkManager/conf.d/00-dhcp-client.conf:
  file.managed:
  - source: salt://network/files/00-dhcp-client.conf
  - makedirs: false
  - mode: 644
  - user: root
  - group: root
  - template: jinja

/lib/systemd/system-sleep/restart_network:
   file.managed:
   - source: salt://network/files/restart_network
   - makedirs: false
   - mode: 755
   - user: root
   - group: root
   - template: jinja

{#
Notes vianova/secure wifi

switch renderer to NetworkManager in /etc/netplan/00-installer-config.yaml
network:
  version: 2
    renderer: NetworkManager

nmcli con add type wifi ifname wlp3s0 con-name work-wifi ssid work-ssid
nmcli con edit id work-wifi
nmcli> set ipv4.method auto
nmcli> set 802-1x.eap peap
nmcli> set 802-1x.phase2-auth mschapv2
nmcli> set 802-1x.identity myusername
nmcli> set 802-1x.password mypassword
nmcli> set wifi-sec.key-mgmt wpa-eap
nmcli> save
nmcli> activate
#}
