{% if 'device' in pillar.conf %}

{% for device in pillar.conf.device %}
/mnt/{{ device }} for rmount:
  file.directory:
  - name: /mnt/{{ device }}
  - user: root
  - group: root
  - makedirs: true
  - mode: 777

fstab entry for /dev/{{ device }}:
  mount.mounted:
  - name: /mnt/{{ device }}
  - device: /dev/{{ device }}
  - mount: false
  - mkmnt: true
  - fstype: auto
  - opts:
    - defaults
    - user
    - noauto
{% endfor %}

{{ grains.bindir }}/rmount:
  file.managed:
  - source: salt://rmount/files/rmount
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja

{% endif %}
