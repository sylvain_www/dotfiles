{{ grains.homedir }}/sandbox:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}

{{ grains.homedir }}/sandbox/new_downloads:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}

{{ grains.homedir }}/sandbox/new_pics:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}
