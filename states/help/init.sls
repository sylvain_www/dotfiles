help board packages:
  pkg.installed:
  - pkgs:
    - conky

{{ grains.homedir }}/.help/background:
  file.managed:
  - source: salt://help/files/background
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - template: jinja

{{ grains.homedir }}/.help/config:
  file.managed:
  - source: salt://help/files/config
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - template: jinja
  - template: jinja

{{ grains.bindir }}/help:
  file.managed:
  - source: salt://help/files/help
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
