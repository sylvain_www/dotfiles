docker repository:
  pkgrepo.managed:
  - humanname: Docker CE
  - name: deb https://download.docker.com/linux/ubuntu jammy stable
  - dist: jammy
  - file: /etc/apt/sources.list.d/docker.list
  - gpgcheck: 1
  - key_url: https://download.docker.com/linux/ubuntu/gpg

docker packages:
  pkg.installed:
  - pkgs:
    - docker-ce
  - require:
    - pkgrepo: docker repository

additional groups for docker user:
  group.present:
  - name: docker
  - addusers:
    - {{ grains.user }}
