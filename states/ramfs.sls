/var/log:
  mount.mounted:
  - device: tmpfs
  - fstype: tmpfs
  - mount: false
  - opts:
    - defaults
    - size=1g
  - match_on:
    - 'name'

/var/tmp:
  mount.mounted:
  - device: tmpfs
  - fstype: tmpfs
  - mount: false
  - opts:
    - defaults
    - size=1g
  - match_on:
    - 'name'
