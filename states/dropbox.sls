/home/dropbox:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true

{{ grains.homedir }}/cloud/dropbox:
  file.symlink:
  - target: /home/dropbox/Dropbox
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true

dropbox installed:
  cmd.run:
  - name: >
      wget -O -
      "https://www.dropbox.com/download?plat=lnx.x86_64"
      | tar xzf -
  - cwd: {{ grains.homedir }}
  - unless:
    - '[ -d "{{ grains.homedir }}/.dropbox-dist" ]'
