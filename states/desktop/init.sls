include:
- git

desktop packages:
  pkg.installed:
  - pkgs:
    - alsa-utils
    - arandr
    - bc
    - dbus-x11
    - rxvt-unicode
    - python-gi-dev
    - xclip
    - xdotool
    - xinput

{% for file in [
  'brightness',
  'browser',
  'calendar',
  'calendarpopup',
  'console',
  'download_phone_medias',
  'dwm_ctrl',
  'dpms',
  'email',
  'explorer',
  'fontsize',
  'keyboard',
  'monitor',
  'mute',
  'note',
  'notify',
  'notify_long_cmd',
  'openlink',
  'osd',
  'popup',
  'rebase',
  'screen_off',
  'statnot_ctrl',
  'terminal',
  'update',
  'view_audio',
  'view_photo',
  'view_video',
  'volume',
] %}
{{ grains.bindir }}/{{ file }}:
  file.managed:
  - source: salt://desktop/files/{{ file }}
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
{% endfor %}

dotfiles user repo checked-out:
  git.cloned:
  - name: git@home.bitbucket.org:sylvain_www/dotfiles
  - user: {{ grains.user }}
  - target: {{ grains.homedir }}/src/bitbucket/sylvain_www/dotfiles
  - require:
    - sls: git

{{ grains.homedir }}/src/bitbucket/sylvain_www/dotfiles/.python-version:
  file.managed:
  - source: salt://desktop/files/python-version
  - template: jinja
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 400
  - require:
    - git: dotfiles user repo checked-out
