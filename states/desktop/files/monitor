#!/usr/bin/python2
import sys
import os
import subprocess
import re

{%- set main = (pillar.conf.outputs|selectattr("name", "equalto", pillar.conf.output)|list)[0] %}

setups = {
    'auto': {
        {%- for output in pillar.conf.outputs %}
        '{{ output.name }}': {
            'w': '{{ output.width }}',
            'h': '{{ output.height }}',
            {%- if 'position' in output %}
            'pos': '{{ output.position }}',
            {%- endif %}
            {%- if 'crtc' in output %}
            'crtc': '{{ output.crtc }}',
            {%- endif %}
        },
        {%- endfor %}
    },
    'single': {
        '{{ main.name }}': { 'w': '{{ main.width }}', 'h': '{{ main.height }}' }
    },
    'copy': {
        {%- for output in pillar.conf.outputs %}
        '{{ output.name }}': {
            'w': '{{ output.width }}',
            'h': '{{ output.height }}',
            {%- if output.name != main.name %}
            'pos': '--same-as {{ main.name }}',
            {%- endif %}
            {%- if 'crtc' in output %}
            'crtc': '{{ output.crtc }}',
            {%- endif %}
        },
        {%- endfor %}
    }
}

def main():
    mode = 'auto' if len(sys.argv) < 2 else sys.argv[1]

    if mode not in setups.keys():
        sys.exit("undefined mode: {}".format(mode))

    status = subprocess.check_output('xrandr').decode('utf-8')

    cmd = "xrandr"
    ret = []
    for line in status.split('\n'):
        match = re.match("(?P<output>^[\w\-]*) (?P<disconnected>dis)?connected",
                         line)
        if match:
            output = match.group('output')
            if output in setups[mode].keys():
                data = setups[mode][output]
                if match.group('disconnected'):
                    cmd += " --output {} --off".format(output)
                else:
                    ret.append(output)
                    cmd += " --output {} --mode {}x{}".format(
                           output, data['w'], data['h'])
                    if 'pos' in data:
                        cmd += " {}".format(data['pos'])
                    if 'crtc' in data:
                        cmd += " --crtc {}".format(data['crtc'])
            else:
                cmd += " --output {} --off".format(output)

    os.system(cmd)
    print("monitor {}: {}".format(mode, " ".join(ret)))

if __name__ == '__main__':
    main()
