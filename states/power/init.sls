power packages:
  pkg.installed:
  - pkgs:
    - pm-utils

additional groups for power:
  group.present:
  - name: powerdev
  - addusers:
    - {{ grains.user }}

sudoer settings for power commands:
  file.managed:
  - name: /etc/sudoers.d/power
  - source: salt://power/files/sudoers
  - makedirs: true
  - mode: 440
  - user: root
  - group: root
  - template: jinja

{% for file in [
    'shutdown',
    'hibernate',
    'reboot',
] %}
{{ grains.bindir }}/{{ file }}:
  file.managed:
  - source: salt://power/files/{{ file }}
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
{% endfor %}

/etc/systemd/system/suspend@.service:
  file.managed:
  - source: salt://power/files/suspend@.service
  - makedirs: true
  - mode: 644
  - user: root
  - group: root
  - template: jinja

  service.enabled:
  - name: suspend@{{ grains.user }}
