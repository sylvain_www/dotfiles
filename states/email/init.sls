include:
- system

{{ grains.bindir }}/check_email:
  file.managed:
  - source: salt://email/files/check_email
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja

run regularly:
  cron.present:
  - name: {{ grains.bindir }}/check_email
  - identifier: email
  - user: {{ grains.user }}
  - minute: '*/2'
  - require:
    - pkg: system packages
    - cron: crontab configured
