eos packages:
  pkg.installed:
  - pkgs:
    - gphoto2

{{ grains.bindir }}/photoshoot:
  file.managed:
  - source: salt://eos/files/photoshoot
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.homedir }}/.gphoto/settings:
  file.managed:
  - source: salt://eos/files/settings
  - makedirs: true
  - mode: 664
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
