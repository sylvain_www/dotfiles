x11 packages:
  pkg.installed:
  - pkgs:
    - xorg
    - xserver-xorg-input-all

{{ grains.homedir }}/.xinitrc:
  file.managed:
  - source: salt://x11/files/xinitrc
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.homedir }}/.xmodmap:
  file.managed:
  - source: salt://x11/files/xmodmap
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.homedir }}/.Xresources:
  file.managed:
  - source: salt://x11/files/Xresources
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.homedir }}/.config/user-dirs.dirs:
  file.managed:
  - source: salt://x11/files/user-dirs.dirs
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - template: jinja

{% for app in [
  'view_audio',
  'view_photo',
  'view_video',
] %}
{{ grains.homedir }}/.local/share/applications/{{ app }}.desktop:
  file.managed:
  - source: salt://x11/files/{{ app }}.desktop
  - makedirs: true
  - mode: 664
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
{% endfor %}

{{ grains.homedir }}/.config/mimeapps.list:
  file.managed:
  - source: salt://x11/files/mimeapps.list
  - makedirs: true
  - mode: 600
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
