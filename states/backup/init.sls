backup packages:
  pkg.installed:
  - pkgs:
    - keyutils
    - ecryptfs-utils

{% set device=pillar.conf.device[0] %}
/mnt/{{ device }} for backup:
  file.directory:
  - name: /mnt/{{ device }}
  - user: root
  - group: root
  - makedirs: true
  - mode: 777

fstab entry for backup on {{ device }}:
  mount.mounted:
  - name: /mnt/{{ device }}/backup
  - device: /mnt/{{ device }}/.private
  - mount: false
  - mkmnt: false
  - fstype: ecryptfs
  - opts:
    - noauto
    - user
    - ecryptfs_cipher=aes
    - ecryptfs_key_bytes=16
    - ecryptfs_sig={{ pillar.private.signature }}
    - ecryptfs_fnek_sig={{ pillar.private.signature }}

{{ grains.bindir }}/backup:
  file.managed:
  - source: salt://backup/files/backup
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - mode: 700
  - makedirs: true
  - template: jinja
