include:
- system

{{ grains.bindir }}/check_battery:
  file.managed:
  - source: salt://battery/files/check_battery
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja

run check_battery regularly:
  cron.present:
  - name: {{ grains.bindir }}/check_battery
  - identifier: battery
  - user: {{ grains.user }}
  - minute: '*/2'
  - require:
    - pkg: system packages
    - cron: crontab configured
