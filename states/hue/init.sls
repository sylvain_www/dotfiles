include:
- python

{{ grains.bindir }}/hue:
  file.managed:
  - source: salt://hue/files/hue
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja

hue virtualenv:
  cmd.run:
  - name: >
      pyenv virtualenvs --bare | grep -q "^hue$"
      || pyenv virtualenv {{ pillar.conf.default_python_version}} hue
  - runas: {{ grains.user }}
  - env:
    - PATH: /usr/bin:{{ grains.homedir }}/.pyenv/bin
    - PYENV_ROOT: {{ grains.homedir }}/.pyenv
  - require:
    - sls: python

hue checked-out:
  git.cloned:
  - name: git@home.bitbucket.org:sylvain_www/hue
  - user: {{ grains.user }}
  - target: {{ grains.homedir }}/src/bitbucket/sylvain_www/hue
  - require:
    - sls: git

hue packages:
  pip.installed:
  - bin_env: {{ grains.homedir }}/.pyenv/versions/hue
  - user: {{ grains.user }}
  - pkgs:
    - phue
  - require:
    - git: hue checked-out
    - cmd: hue virtualenv
