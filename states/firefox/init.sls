apt priority over snap:
  file.managed:
  - source: salt://firefox/files/apt_priority
  - name: /etc/apt/preferences.d/99mozillateamppa
  - user: root
  - group: root
  - mode: 644
  - makedirs: True

mozilla repository:
  pkgrepo.managed:
  - humanname: Mozilla Firefox
  - name: deb https://ppa.launchpadcontent.net/mozillateam/ppa/ubuntu/ jammy main
  - file: /etc/apt/sources.list.d/mozillateam-ubuntu-ppa-jammy.list
  - keyid: 9BDB3D89CE49EC21
  - keyserver: keyserver.ubuntu.com

firefox installed:
  pkg.installed:
  - pkgs:
    - firefox
  - require:
    - file: apt priority over snap
    - pkgrepo: mozilla repository

{{ grains.homedir }}/.vimperatorrc:
  file.managed:
  - source: salt://firefox/files/vimperatorrc
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

allow openlink in firefox apparmor config:
  file.managed:
  - name: {{ grains.builddir }}/firefox_apparmor.patch
  - source: salt://firefox/files/apparmor.patch
  - user: {{ grains.user }}
  - group: {{ grains.user }}

  cmd.run:
  - name: grep -q openlink /etc/apparmor.d/usr.bin.firefox || patch --forward -d /etc/apparmor.d -p0 < {{ grains.builddir }}/firefox_apparmor.patch
  - require:
    - pkg: firefox installed

{#
TODO: automatize
- create 2 profiles: default and email
- synchronize accounts via mozilla sync
- change firefox secret settings (about:config):
   - browser.urlbar.clickSelectsAll true
   - browser.urlbar.doubleClickSelectsAll false
- for email profile, setup external links:
   - patch handler.json (add `link` mimetype)
   - restart
   - in about:config, create a new entries:
       - network.protocol-handler.expose.link: false
       - network.protocol-handler.external.link: true
       - network.protocol-handler.warn-external.link: false
       - gecko.handlerService.schemes.link.0.name: openlink
   - copy gm_scripts to profile dir (rewrite links)

- update add-on "Window Title": main browser / email browser

Snap related issues:
- won't boot, until cgroups v2 fixed by snap, add in /etc/default/grub:
GRUB_CMDLINE_LINUX="systemd.unified_cgroup_hierarchy=0"

- cursor theme not applied

- email links broken (seem broken in apt version too): cannot open external app

#}
