// ==UserScript==
// @name        openlink
// @namespace   sylvain.www@gmail.com
// @description redirect links to openlink
// @include     https://mail.google.com/*
// @version     1
// @grant       none
// allow pasting
// ==/UserScript==

window.addEventListener('click', function(event) {
  for (var elem = event.target ; elem && elem !== document; elem = elem.parentNode ) {
    if (elem.tagName.toLowerCase() === 'a') {
      if(elem.href.toLowerCase().indexOf("mail.google.com") === -1 &&
         elem.href.toLowerCase().indexOf("slack.com") === -1
      ) {
        if(elem.href.indexOf("http") == 0) {
          window.open('link://' + elem.href, "_self");
          event.stopPropagation();
          event.preventDefault();
        }
      }
      break;
    }
  }
}, true);
