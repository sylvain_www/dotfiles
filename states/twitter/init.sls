include:
- desktop
- ruby
- system

twitter packages:
  gem.installed:
  - name: t
  - require:
    - pkg: ruby packages
    - pkg: system packages

{{ grains.homedir }}/.trc:
  file.managed:
  - source: salt://twitter/files/trc
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.bindir }}/check_twitter:
  file.managed:
  - source: salt://twitter/files/check_twitter
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja

run twitter check regularly:
  cron.present:
  - name: {{ grains.bindir }}/check_twitter
  - identifier: twitter
  - user: {{ grains.user }}
  - minute: '*/2'
  - require:
    - pkg: desktop packages
    - cron: crontab configured
