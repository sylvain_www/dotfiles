include:
- builddir
- git
- system

remote packages:
  pkg.installed:
  - pkgs:
    - golang
    - libxtst-dev

remote checked-out:
  git.latest:
  - name: https://bitbucket.org/sylvain_www/android-remote-control
  - user: {{ grains.user }}
  - target: {{ grains.builddir }}/remote
  - force_checkout: True
  - force_fetch: True
  - force_reset: True
  - require:
    - sls: git

remote compiled:
  cmd.run:
  - name: make -C {{ grains.builddir }}/remote/host
  - require:
    - git: remote checked-out
    - pkg: remote packages
    - pkg: system packages

{{ grains.bindir }}/remotecontrol:
  file.managed:
  - source: {{ grains.builddir }}/remote/host/bin/server-linux-amd64.out
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - encoding_errors: ignore
  - mode: 700
  - makedirs: True
  - require:
    - cmd: remote compiled
