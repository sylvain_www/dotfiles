plymouth packages:
  pkg.installed:
  - pkgs:
    - plymouth
    - plymouth-themes
    - plymouth-label

/usr/share/plymouth/themes/cleansplash:
  file.recurse:
  - source: salt://plymouth/files/cleansplash
  - user: root
  - group: root
  - template: jinja

install plymouth alternative:
  alternatives.install:
  - name: default.plymouth
  - link: /usr/share/plymouth/themes/default.plymouth
  - path: /usr/share/plymouth/themes/cleansplash/cleansplash.plymouth
  - priority: 200

set plymouth alternative:
  alternatives.set:
  - name: default.plymouth
  - path: /usr/share/plymouth/themes/cleansplash/cleansplash.plymouth

  cmd.run:
  - name: update-initramfs -u
  - onchanges:
    - alternatives: default.plymouth

grub settings for plymouth:
  file.managed:
  - name: /etc/default/grub.d/90-plymouth-settings.cfg
  - source: salt://plymouth/files/90-plymouth-settings.cfg
  - user: root
  - group: root
  - template: jinja

update grub for plymouth:
  cmd.run:
  - name: update-grub
  - onchanges:
    - file: grub settings for plymouth
