music packages:
  pkg.installed:
  - pkgs:
    - ncmpcpp
    - mpc
    - mpd
    - python3-mpd

{{ grains.homedir }}/.mpd:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}

conflicting default mpd conf removed:
  file.absent:
  - name: /etc/init.d/mpd

  service.disabled:
  - name: mpd

{{ grains.homedir }}/.mpd/lyrics:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}

{{ grains.homedir }}/music/.playlists:
  file.directory:
  - makedirs: true
  - user: {{ grains.user }}
  - group: {{ grains.user }}

{{ grains.homedir }}/.mpdconf:
  file.managed:
  - source: salt://music/files/mpdconf
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.homedir }}/.ncmpcpp/config:
  file.managed:
  - source: salt://music/files/ncmpcpp_config
  - makedirs: true
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.homedir }}/.ncmpcpp/bindings:
  file.managed:
  - source: salt://music/files/ncmpcpp_bindings
  - makedirs: true
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{% for file in [
  'next', 'play', 'player', 'prev', 'seek', 'stop', 'check_song'
] %}
{{ grains.bindir }}/{{ file }}:
  file.managed:
  - source: salt://music/files/bin/{{ file }}
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
{% endfor %}
