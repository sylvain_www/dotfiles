include:
- zsh

user configured:
  user.present:
  - name: {{ grains.user }}
  - shell: /bin/zsh
  - require:
    - pkg: zsh packages
