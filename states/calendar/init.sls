include:
- python
- system

calendar packages:
  pkg.installed:
  - pkgs:
    - gcalcli

  pip.installed:
  - pkgs:
    - oauth2client
  - require:
    - sls: python

{{ grains.bindir }}/check_calendar:
  file.managed:
  - source: salt://calendar/files/check_calendar
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja

run calendar check regularly:
  cron.present:
  - name: {{ grains.bindir }}/check_calendar
  - identifier: calendar
  - user: {{ grains.user }}
  - minute: '*/20'
  - require:
    - pkg: system packages
    - cron: crontab configured
