status board packages:
  pkg.installed:
  - pkgs:
    - conky

{{ grains.homedir }}/.status/background:
  file.managed:
  - source: salt://status/files/background
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - template: jinja

{{ grains.homedir }}/.status/config:
  file.managed:
  - source: salt://status/files/config
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - template: jinja
  - template: jinja

{{ grains.bindir }}/status:
  file.managed:
  - source: salt://status/files/status
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
