include:
- system

oracle packages:
  pkg.installed:
  - pkgs:
    - bsdmainutils
    - fortune-mod
    - calendar

{{ grains.bindir }}/oracle:
  file.managed:
  - source: salt://oracle/files/oracle
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedir: true
  - mode: 700
  - template: jinja

run oracle regularly:
  cron.present:
  - name: {{ grains.bindir }}/oracle
  - identifier: oracle
  - user: {{ grains.user }}
  - minute: '*/20'
  - require:
    - pkg: system packages
    - cron: crontab configured
