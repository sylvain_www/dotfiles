{{ grains.homedir }}/.ssh/config:
  file.managed:
  - source: salt://ssh/files/config
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 600
  - template: jinja
