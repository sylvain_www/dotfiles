#!/usr/bin/env python3

import argparse
import libkeepass
import os
import sys


def get_password(password, passfile):
    if password:
        return password

    try:
        with open(passfile, 'r') as fp:
            return fp.read().rstrip()
    except IOError:
        sys.exit('error: provide either password or passfile')


def get_root(filename, password):
    try:
        with libkeepass.open(filename, password=password) as kdb:
            return kdb.obj_root.getroottree()
    except IOError:
        if not os.path.isfile(filename):
            sys.exit('error: no such file: ' + filename)
        sys.exit('error: cannot open database (check password): ' + filename)


def get_elements(root, searchgroup, searchkey, searchvalue, exact_match=False):
    if exact_match:
        xquery = ".//Group{group}/Entry/String[Key='{key}' and Value='{value}']/..".format(
            group="[Name='{group}']".format(group=searchgroup) if searchgroup else '',
            key=searchkey,
            value=searchvalue
        )
    else:
        xquery = ".//Group{group}/Entry/String[Key='{key}' and contains(Value, '{value}')]/..".format(
            group="[Name='{group}']".format(group=searchgroup) if searchgroup else '',
            key=searchkey,
            value=searchvalue
        )
    return root.xpath(xquery)


def put(key, elts, fields, batch=False, show_password=False):
    if len(elts) == 0:
        sys.exit("error: no match found")

    if batch:
        if len(fields) != 1:
            sys.exit("error: multiple outputs is not compatible with batch mode:\n{fields}".format(
                fields=", ".join(fields)))

        if len(elts) != 1:
            msg = []
            for index, elt in enumerate(elts):
                msg.append("{index}. {value} ({group})".format(
                    index=index,
                    value=elt.find("./String[Key='{key}']/Value".format(key=key)),
                    group=elt.find("../Name".format(key=key))
                ))
            sys.exit("error: multiple matches and batch mode:\n{msg}".format(msg="\n".join(msg)))

        print(elts[0].find("./String[Key='{field}']/Value".format(field=fields[0])))
        sys.exit()

    print('---')
    for elt in elts:
        for field in fields:
            if field == 'Password' and not show_password:
                value = '*****'
            elif field == 'Group':
                value=elt.find("../Name")
            else:
                value=elt.find("./String[Key='{field}']/Value".format(field=field))
            print("{key}: {value}".format(
                key=field.lower(),
                value=value
            ))
        print('---')


def main():
    parser=argparse.ArgumentParser(description="")

    default_dbfile = os.path.expanduser('~/.database.kdbx')
    default_passfile = os.path.expanduser('~/.database.password')
    default_outputs = ['Title', 'Group', 'UserName', 'Password', 'URL', 'Notes']

    parser.add_argument('--batch', '-b', action='store_true')
    parser.add_argument('--exact-match', '-e', action='store_true')
    parser.add_argument('--show-password', '-s', action='store_true')
    parser.add_argument('--dbfile', '-d', default=default_dbfile)
    parser.add_argument('--passfile', '-f', default=default_passfile)
    parser.add_argument('--password', '-p')
    parser.add_argument('--group', '-g')
    parser.add_argument('--key', '-k', choices=default_outputs, default='Title')
    parser.add_argument('--output', '-o', choices=default_outputs, action='append', default=[])
    parser.add_argument('searchitem')
    args = parser.parse_args()

    if not args.output:
        if args.batch:
            args.output = ['Password']
        else:
            args.output = default_outputs

    masterpass = get_password(args.password, args.passfile)
    root = get_root(args.dbfile, masterpass)
    elts = []
    if args.batch or args.exact_match:
        elts = get_elements(root, args.group, args.key, args.searchitem, True)
    if not elts and not args.exact_match:
        elts = get_elements(root, args.group, args.key, args.searchitem, False)
    put(args.key, elts, args.output, args.batch, args.show_password)


if __name__ == '__main__':
    main()
