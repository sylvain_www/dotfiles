include:
- python
- system

password packages:
  pkg.installed:
  - pkgs:
    - keepassx
    - xsel

  pip.installed:
  - pkgs:
    - libkeepass
  - require:
    - sls: python

{{ grains.homedir }}/.config/keepassx/keepassx2.ini:
  file.managed:
  - source: salt://password/files/keepassx2.ini
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - template: jinja

{% for file in [
    'pass',
    'pass_ask',
    'password',
] %}
{{ grains.bindir }}/{{ file }}:
  file.managed:
  - source: salt://password/files/{{ file }}
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja
{% endfor %}

{{ grains.homedir }}/.database.kdbx:
  file.symlink:
    - target: {{ grains.homedir }}/cloud/dropbox/database.kdbx
