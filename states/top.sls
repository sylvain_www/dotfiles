base:
  '*':
  - backup
  - builddir
  - calendar
  - chrome
  - contacts
  - cursor
  - desktop
  - dropbox
  - dwm
  - email
  - extras
  - firefox
  - flatpak
  - gdrive
  - git
  - gnupg
  - headset
  - help
  - lock
  - menu
  - music
  - network
  - oracle
  - password
  - plymouth
  - power
  - python
  - radio
  - ramfs
  - resume
  - rmount
  - rust
  - sandbox
  - screenshot
  - scripts
  - slim
  - sms
  - ssh
  - statnot
  - status
  - system
  - tmux
  - twitter
  - user
  - vifm
  - vim
  - vpn
  - qarma
  - wallpaper
  - x11
  - zathura
  - zsh

  akasa:
  - album
  - android
  - docker
  - eos
  - gdrive.home
  - grisbi
  - hue
  - photos
  - private
  - recipe
  - recos
  - remote
  - scripts.scality
  - scripts.vianova
  - zsh.android
  - zsh.scality
  - zsh.vianova

  e7470:
  - battery
  - docker
  - gdrive.home
  - gdrive.scality
  - scripts.scality
  - touchpad
  - zsh.scality

  l7400:
  - battery
  - docker
  - gdrive.home
  - scripts.vianova
  - touchpad
  - zsh.vianova

  eeepc:
  - battery
  - gdrive.home
  - touchpad
