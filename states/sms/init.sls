{{ grains.bindir }}/sms:
  file.managed:
  - source: salt://sms/files/sms
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - mode: 700
  - template: jinja
