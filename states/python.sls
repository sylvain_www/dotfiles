include:
- builddir
- git

python packages:
  pkg.installed:
  - pkgs:
    - python3
    - python3-dev
    - python3-pip

docker-py:
  pip.installed:
    - require:
      - pkg: python packages

pyenv build packages:
  pkg.installed:
  - pkgs:
    - build-essential
    - curl
    - libbz2-dev
    - libffi-dev
    - liblzma-dev
    - libncurses5-dev
    - libreadline-dev
    - libsqlite3-dev
    - libssl-dev
    - libxml2-dev
    - libxmlsec1-dev
    - llvm
    - make
    - tk-dev
    - wget
    - xz-utils
    - zlib1g-dev

pyenv checked out:
  git.latest:
  - name: https://github.com/pyenv/pyenv.git
  - user: {{ grains.user }}
  - target: {{ grains.homedir }}/.pyenv
  - force_checkout: True
  - force_fetch: True
  - force_reset: True
  - require:
    - sls: git

pyenv virtualenv plugin checked out:
  git.latest:
  - name: https://github.com/pyenv/pyenv-virtualenv.git
  - user: {{ grains.user }}
  - target: {{ grains.homedir }}/.pyenv/plugins/pyenv-virtualenv
  - force_checkout: True
  - force_fetch: True
  - force_reset: True
  - require:
    - sls: git
    - git: pyenv checked out

pyenv compiled and default python installed:
  pyenv.installed:
  - name: {{ pillar.conf.default_python_version }}
  - default: True
  - user: {{ grains.user }}
  - require:
    - pkg: pyenv build packages
    - git: pyenv checked out
