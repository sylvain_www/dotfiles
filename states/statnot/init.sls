include:
- builddir
- git
- python

statnot packages:
  pip.installed:
  - pkgs:
    - dbussy
    - python-mpd2
  - require:
    - sls: python

statnot checked-out:
  git.latest:
  - name: git@home.bitbucket.org:sylvain_www/statnot
  - user: {{ grains.user }}
  - target: {{ grains.builddir }}/statnot
  - force_checkout: True
  - force_fetch: True
  - force_reset: True
  - require:
    - sls: git

{{ grains.bindir }}/statnot:
  file.managed:
  - source: {{ grains.builddir }}/statnot/statnot
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - mode: 700
  - makedirs: True
  - require:
    - git: statnot checked-out
    - sls: python

statnot user repo checked-out:
  git.cloned:
  - name: git@home.bitbucket.org:sylvain_www/statnot
  - user: {{ grains.user }}
  - target: {{ grains.homedir }}/src/bitbucket/sylvain_www/statnot
  - require:
    - sls: git
