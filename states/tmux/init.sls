tmux packages:
  pkg.installed:
  - pkgs:
    - tmux

{{ grains.homedir }}/.tmux.conf:
  file.managed:
  - source: salt://tmux/files/tmux.conf
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.bindir }}/tmux_ctrl:
  file.managed:
  - source: salt://tmux/files/tmux_ctrl
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
