menu packages:
  pkg.installed:
  - pkgs:
    - suckless-tools

{{ grains.homedir }}/.cache:
  file.directory:
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700

{{ grains.homedir }}/.cache/dmenu_recent:
  file.managed:
  - source: salt://menu/files/dmenu_recent
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - template: jinja

{{ grains.bindir }}/menu:
  file.managed:
  - source: salt://menu/files/menu
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja
