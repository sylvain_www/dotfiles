include:
- python
- system

{{ grains.bindir }}/contacts:
  file.managed:
  - source: salt://contacts/files/contacts
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja

{{ grains.homedir }}/.contacts/client_secrets.json:
  file.managed:
  - source: salt://contacts/files/client_secrets.json
  - template: jinja
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 400

{{ grains.homedir }}/.contacts/.python-version:
  file.managed:
  - source: salt://contacts/files/python-version
  - template: jinja
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 400

contacts virtualenv:
  cmd.run:
  - name: >
      pyenv virtualenvs --bare | grep -q "^contacts$"
      || pyenv virtualenv {{ pillar.conf.default_python_version}} contacts
  - runas: {{ grains.user }}
  - env:
    - PATH: /usr/bin:{{ grains.homedir }}/.pyenv/bin
    - PYENV_ROOT: {{ grains.homedir }}/.pyenv
  - require:
    - sls: python

contacts checked-out:
  git.cloned:
  - name: git@home.bitbucket.org:sylvain_www/contacts
  - user: {{ grains.user }}
  - target: {{ grains.homedir }}/src/bitbucket/sylvain_www/contacts
  - require:
    - sls: git

contacts packages:
  pip.installed:
  - bin_env: {{ grains.homedir }}/.pyenv/versions/contacts
  - user: {{ grains.user }}
  - requirements: {{ grains.homedir }}/src/bitbucket/sylvain_www/contacts/requirements.txt
  - require:
    - git: contacts checked-out
    - cmd: contacts virtualenv

{{ grains.bindir }}/check_birthdays:
  file.managed:
  - source: salt://contacts/files/check_birthdays
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700
  - template: jinja

run contacts check regularly:
  cron.present:
  - name: {{ grains.bindir }}/check_birthdays
  - identifier: birthdays
  - user: {{ grains.user }}
  - minute: '*/60'
  - require:
    - pkg: system packages
    - cron: crontab configured
