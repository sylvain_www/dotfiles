/usr/share/slim/themes/cleansplash:
  file.recurse:
  - source: salt://slim_theme/files/cleansplash
  - encoding_errors: ignore
  - user: root
  - group: root
  - template: jinja
