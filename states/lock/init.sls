include:
- builddir
- git
- slim_theme
- system

lock packages:
  pkg.installed:
  - pkgs:
    - fontconfig
    - libimlib2-dev
    - libpam0g-dev
    - libxft-dev
    - libxrandr-dev
    {% if pillar.conf.autolock == 'on' %}
    - xautolock
    {% endif %}

slimlock checked-out:
  git.latest:
  - name: https://github.com/dannyn/slimlock.git
  - user: {{ grains.user }}
  - target: {{ grains.builddir }}/slimlock
  - force_checkout: True
  - force_fetch: True
  - force_reset: True
  - require:
    - sls: git

slimlock patched:
  file.managed:
  - name: {{ grains.builddir }}/slimlock/panel.patch
  - source: salt://lock/files/panel.cpp.patch0_12
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - require:
    - git: slimlock checked-out

  cmd.run:
  - name: patch -p1 < panel.patch
  - cwd: {{ grains.builddir }}/slimlock
  - require:
    - git: slimlock checked-out

slimlock compiled:
  cmd.run:
  - name: make -C {{ grains.builddir }}/slimlock
  - require:
    - cmd: slimlock patched
    - pkg: lock packages
    - pkg: system packages

{{ grains.bindir }}/slimlock:
  file.managed:
  - source: {{ grains.builddir }}/slimlock/slimlock
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - mode: 700
  - makedirs: True
  - require:
    - cmd: slimlock compiled

/etc/slimlock.conf:
  file.managed:
  - source: salt://lock/files/slimlock.conf
  - user: root
  - group: root
  - mode: 644
  - makedirs: True

{{ grains.bindir }}/lock:
  file.managed:
  - source: salt://lock/files/lock
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - mode: 700
  - makedirs: true
  - template: jinja
