zathura packages:
  pkg.installed:
  - pkgs:
    - zathura

{{ grains.homedir }}/.config/zathura/zathurarc:
  file.managed:
  - source: salt://zathura/files/zathurarc
  - makedirs: true
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
