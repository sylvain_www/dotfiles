include:
- builddir
- git
- system

recipe packages:
  pkg.installed:
  - pkgs:
    - gettext
    #- libncursesw5-dev TODO

recipe checked-out:
  git.latest:
  - name: https://bitbucket.org/sylvain_www/pyrecipes
  - user: {{ grains.user }}
  - target: {{ grains.builddir }}/recipe
  - force_checkout: True
  - force_fetch: True
  - force_reset: True
  - require:
    - sls: git

locale message directory exists english:
  file.directory:
    - name: /usr/share/locale-langpack/en/LC_MESSAGES
    - makedirs: True

locale message directory exists french:
  file.directory:
    - name: /usr/share/locale-langpack/fr/LC_MESSAGES
    - makedirs: True

recipe installed:
  cmd.run:
  - name: make install -C {{ grains.builddir }}/recipe
  - require:
    - git: recipe checked-out
    - pkg: recipe packages
    - pkg: system packages
    - file: locale message directory exists english
    - file: locale message directory exists french

{{ grains.homedir }}/.pyrecipesrc:
  file.managed:
  - source: salt://recipe/files/pyrecipesrc
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.bindir }}/recipes:
  file.managed:
  - source: salt://recipe/files/recipes
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
