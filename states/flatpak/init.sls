# Flatpak still in dev in Salt 3004, hence commented code below
# uncomment when ready
# install packages manually till then

flatpak packages:
  pkg.installed:
  - pkgs:
    - flatpak

flathub repo:
  #  flatpak.add_remote:
  #  - name: flathub
  #  - location: https://flathub.org/repo/flathub.flatpakrepo
  #
  # temp code:
  cmd.run:
  - name: >
      flatpak remote-add
      --if-not-exists
      flathub https://flathub.org/repo/flathub.flatpakrepo
  - require:
    - pkg: flatpak packages

{%- for pname, plist in pillar.flatpak.items() %}
{%- for package in plist %}

flatpak package {{ pname }} {{ package.name }}:
  #  flatpak.installed:
  #  - location: flathub
  #  - name: {{ package.name }}
  #
  # temp code:
  cmd.run:
  - name: flatpak install --assumeyes {{ package.name }}
  - require:
    - pkg: flatpak packages

{{ grains.bindir }}/{{ package.exe }}:
  file.managed:
  - source: salt://flatpak/files/exec.sh
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
  - defaults:
     package: {{ package.name }}

{%- endfor %}
{%- endfor %}
