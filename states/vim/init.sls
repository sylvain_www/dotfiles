{{ grains.homedir }}/.vimrc:
  file.managed:
  - source: salt://vim/files/vimrc
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja

{{ grains.homedir }}/.vim:
  file.recurse:
  - source: salt://vim/files/vim
  - user: {{ grains.user }}
  - group: {{ grains.user }}
