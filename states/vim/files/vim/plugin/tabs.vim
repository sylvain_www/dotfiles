"define our text-style tabline
"see :help setting-tabline
function MyTabLine()
    let s = ''
    let i = 1
    let tabtitles = []
    "compute space visible for filenames (remove space for left/right indicators)
    let usablecolumns = getbufvar(1, '&columns') - 2
    while i <= tabpagenr('$')
        let buflist = tabpagebuflist(i)
        let winnr = tabpagewinnr(i)
        let buflen = len(buflist)
        if buflen == 1
            let file = bufname(buflist[winnr - 1])
            let file = fnamemodify(file, ':p:t')
        else
            let file = '[multi]'
        endif
        if file == ''
            let file = '[no name]'
        endif
        let file = '[' . i . ']' . file
        while buflen > 0
            if getbufvar(buflist[buflen - 1], "&mod")
                let file .= '+  '
                break
            endif
            let buflen -= 1
        endwhile
        if buflen == 0
            let file .= '   '
        endif
        let tabtitles += [ file ]
        let i += 1
    endwhile
    "determine first visible tab
    let i = tabpagenr()
    let linelen = 0
    while i > 0
        let linelen += strlen(tabtitles[i - 1])
        if linelen > usablecolumns
            break
        endif
        let i -= 1
    endwhile
    let firsttab = i
    "if first visible tag is the first one, build from the left
    if firsttab == 0
        let i = 1
        let linelen = 0
        let s = '%2* '
        "build the tabline
        while i <= tabpagenr('$')
            let file = tabtitles[i-1]
            let linelen += strlen(file)
            "to allow mouse selection:
            let s .= '%' . i . 'T'
            let s .= (tabpagenr() == i ? '%1*' : '%2*')
            if linelen > usablecolumns
                let s .= strpart(file, 0, strlen(file) - (linelen - usablecolumns))
                break
            else
                let s .= file
            endif
            let i += 1
        endwhile
        let s .= '%2*%='
        let s .= (i < tabpagenr('$')? '%2*>%*' : '%*')

    "otherwise build from the right
    else
        let i = tabpagenr()
        let linelen = 0
        let s = (i < tabpagenr('$')? '%2*>%*' : '%*') . s
        let s = '%2*%=' . s
        "build the tabline
        while i > 0
            let file = tabtitles[i-1]
            let linelen += strlen(file)
            "to allow mouse selection:
            if linelen > usablecolumns
                let s = strpart(file, linelen - usablecolumns) . s
                break
            else
                let s = file . s
            endif
            let s = (tabpagenr() == i ? '%1*' : '%2*') . s
            let s = '%' . i . 'T' . s
            let i -= 1
        endwhile
        let s = '%2*<' . s

    endif
    return s
endfunction
set showtabline=2
set tabline=%!MyTabLine()

