" Vim color file

set background=dark

hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "mycolors"


"" general colors
hi ColorColumn      ctermfg=001     ctermbg=none    cterm=none
hi CursorColumn     ctermfg=none    ctermbg=008     cterm=none
hi CursorLine       ctermfg=none    ctermbg=008     cterm=none
hi Cursor           ctermfg=none    ctermbg=none    cterm=none
hi Directory        ctermfg=013     ctermbg=none    cterm=bold
hi diffAdd          ctermfg=002     ctermbg=236     cterm=none
hi diffDelete       ctermfg=001     ctermbg=236     cterm=none
hi diffChange       ctermfg=none    ctermbg=none    cterm=none
hi diffText         ctermfg=003     ctermbg=236     cterm=none
hi ErrorMsg         ctermfg=001     ctermbg=none    cterm=none
hi VertSplit        ctermfg=008     ctermbg=008     cterm=none
hi Folded           ctermfg=014     ctermbg=none    cterm=none
hi FoldColumn       ctermfg=014     ctermbg=none    cterm=none
hi IncSearch        ctermfg=234     ctermbg=002     cterm=none
hi LineNr           ctermfg=239     ctermbg=none    cterm=none
hi CursorLineNr     ctermfg=012     ctermbg=none    cterm=none
hi MatchParen       ctermfg=234     ctermbg=002     cterm=none
hi ModeMsg          ctermfg=014     ctermbg=none    cterm=none
hi NonText          ctermfg=239     ctermbg=none    cterm=none
hi Normal           ctermfg=014     ctermbg=none    cterm=none
hi Pmenu            ctermfg=014     ctermbg=008     cterm=none
hi PmenuSel         ctermfg=003     ctermbg=008     cterm=none
hi PmenuSbar        ctermfg=234     ctermbg=014     cterm=none
hi PmenuThumb       ctermfg=014     ctermbg=008     cterm=none
hi Question         ctermfg=009     ctermbg=none    cterm=none
hi Search           ctermfg=234     ctermbg=002     cterm=none
hi SpecialKey       ctermfg=239     ctermbg=none    cterm=none
hi StatusLine       ctermfg=none    ctermbg=008     cterm=none
hi StatusLineNC     ctermfg=239     ctermbg=008     cterm=none
hi TabLine          ctermfg=none    ctermbg=008     cterm=none
hi TabLineFill      ctermfg=none    ctermbg=008     cterm=none
hi Visual           ctermfg=234     ctermbg=003     cterm=none
hi VisualNOS        ctermfg=234     ctermbg=003     cterm=none
hi WarningMsg       ctermfg=009     ctermbg=none    cterm=none
hi WildMenu         ctermfg=003     ctermbg=008     cterm=none
" syntax highlighting
hi Comment          ctermfg=002     ctermbg=none    cterm=none
hi Constant         ctermfg=101     ctermbg=none    cterm=none
hi Identifier       ctermfg=003     ctermbg=none    cterm=none
hi Statement        ctermfg=067     ctermbg=none    cterm=none
hi PreProc          ctermfg=096     ctermbg=none    cterm=none
hi Type             ctermfg=103     ctermbg=none    cterm=none
hi Special          ctermfg=100     ctermbg=none    cterm=none

hi Underlined       ctermfg=none    ctermbg=none    cterm=none
hi Ignore           ctermfg=none    ctermbg=none    cterm=none
hi Error            ctermfg=001     ctermbg=none    cterm=none
hi Todo             ctermfg=234     ctermbg=001     cterm=bold

" overwrites for light background
if inverted_video == '1'
hi Normal           ctermfg=008     ctermbg=none    cterm=none
hi LineNr           ctermfg=246     ctermbg=none    cterm=none
hi Comment          ctermfg=028     ctermbg=none    cterm=none
hi Constant         ctermfg=025     ctermbg=none    cterm=none
hi Identifier       ctermfg=089     ctermbg=none    cterm=none
hi Statement        ctermfg=018     ctermbg=none    cterm=none
hi Type             ctermfg=088     ctermbg=none    cterm=none
hi Special          ctermfg=239     ctermbg=none    cterm=none
endif
