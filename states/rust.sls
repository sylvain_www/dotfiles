{{ grains.homedir }}/.cargo/bin:
  file.directory:
  - user: root
  - group: root
  - makedirs: true
  - mode: 777

# install rustup:
# curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
