include:
- python
- system

{{ grains.bindir }}/album:
  file.managed:
  - source: salt://album/files/album
  - template: jinja
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 700

photos album checked-out:
  git.cloned:
  - name: git@home.bitbucket.org:sylvain_www/photos_album
  - user: {{ grains.user }}
  - target: {{ grains.homedir }}/src/bitbucket/sylvain_www/photos_album
  - require:
    - sls: git

photos album virtualenv:
  cmd.run:
  - name: >
      pyenv virtualenvs --bare | grep -q "^album$"
      || pyenv virtualenv {{ pillar.conf.default_python_version}} album
  - runas: {{ grains.user }}
  - env:
    - PATH: /usr/bin:{{ grains.homedir }}/.pyenv/bin
    - PYENV_ROOT: {{ grains.homedir }}/.pyenv
  - require:
    - sls: python

photos album packages:
  pip.installed:
  - bin_env: {{ grains.homedir }}/.pyenv/versions/album
  - user: {{ grains.user }}
  - requirements: {{ grains.homedir }}/src/bitbucket/sylvain_www/photos_album/requirements.txt
  - require:
    - git: photos album checked-out
    - cmd: photos album virtualenv

{{ grains.homedir }}/src/bitbucket/sylvain_www/photos_album/.python-version:
  file.managed:
  - source: salt://album/files/python-version
  - template: jinja
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - makedirs: true
  - mode: 400
  - require:
    - git: photos album checked-out
