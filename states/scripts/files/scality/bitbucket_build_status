#!/bin/bash
#
# utility script to set build status on bitbucket repos
#

set -o errexit -o pipefail

REPO=""
SHA1=""
USERNAME=""
PASSWORD=""
KEY="pre-merge"
STATUS="FAILED"
MESSAGE="manual build status"
URL="https://www.youtube.com/watch?v=dQw4w9WgXcQ"

debug=0

usage() {
    cat <<-EOF

		Usage: $0 [options] <repo> <sha1> <username> <password>

			<repo>:			bitbucket repository: owner/slug
			<sha1>:			commit to set status on
			<username>:		credentials
			<password>:		credentials
			[-k|--key]:		customize key (default: pre-merge)
			[-s|--status]:		customize status (default: FAILED)
			[-m|--message]:		customize message
			[-u|--url]:		customize url
			[-d|--debug]:		show debug messages
			[-h|--help]:		show this help and exit
	EOF
}

error() {
    usage
    echo ""
    echo "$1" >&2
    exit 1
}

while [ -n "$1" ]; do
    case "$1" in
        -k|--key)
            KEY=$2
            shift 2
            ;;
        -s|--status)
            STATUS=$2
            shift 2
            ;;
        -m|--message)
            MESSAGE=$2
            shift 2
            ;;
        -u|--url)
            URL=$2
            shift 2
            ;;
        -d|--debug)
            debug=1
            shift
            ;;
        -h|--help)
            usage
            exit 0
            ;;
        *)
            if [ -z "$REPO" ]; then
                REPO="$1"
            elif [ -z "${SHA1}" ]; then
                SHA1="$1"
            elif [ -z "${USERNAME}" ]; then
                USERNAME="$1"
            elif [ -z "${PASSWORD}" ]; then
                PASSWORD="$1"
            else
                log_error "Unexpected argument \"$1\""
                exit 1
            fi
            shift
            ;;
    esac
done

test $debug -eq 1 && set -o xtrace -o verbose
test -z "$REPO" && error "missing repository"
test -z "$SHA1" && error "missing commit id"
test -z "$USERNAME" && error "missing username"
test -z "$PASSWORD" && error "missing password"

test "$STATUS" = "FAILED" || \
test "$STATUS" = "SUCCESSFUL" || \
test "$STATUS" = "INPROGRESS" || \
test "$STATUS" = "STOPPED" || \
error "unknown key: $STATUS (expected SUCCESSFUL, FAILED, INPROGRESS or STOPPED)"

cleanup() {
    rm -f build.json
}
trap cleanup EXIT


BB_URL="https://api.bitbucket.org"
BUILD_API="$BB_URL/2.0/repositories/$REPO/commit/$SHA1/statuses/build"

cat > build.json << EOF
{
    "state": "$STATUS",
    "key": "$KEY",
    "name": "$MESSAGE",
    "url": "$URL"
}
EOF

echo "setting status $STATUS on key $KEY"
curl -s -f \
     -u "$USERNAME:$PASSWORD" \
     -X POST $BUILD_API \
     -H "Content-Type: application/json" \
     -d @build.json > /dev/null \
     && echo 'OK' || echo 'fail'
