{% for file in [
  'pipdep',
] %}
{{ grains.bindir }}/.{{ file }}:
  file.managed:
  - source: salt://scripts/files/scality/{{ file }}
  - makedirs: true
  - mode: 600
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
{% endfor %}

{% for file in [
  'bitbucket_build_status',
  'build',
  'eve_api_client',
] %}
{{ grains.bindir }}/{{ file }}:
  file.managed:
  - source: salt://scripts/files/scality/{{ file }}
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
{% endfor %}
