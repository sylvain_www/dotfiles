{% for file in [
  'aws_renew',
  'api',
] %}
{{ grains.bindir }}/{{ file }}:
  file.managed:
  - source: salt://scripts/files/vianova/{{ file }}
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
{% endfor %}
