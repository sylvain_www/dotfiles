{% for file in [
  'spectrum',
  'spectrum_bash',
  'wipe_docker',
] %}
{{ grains.bindir }}/{{ file }}:
  file.managed:
  - source: salt://scripts/files/common/{{ file }}
  - makedirs: true
  - mode: 700
  - user: {{ grains.user }}
  - group: {{ grains.user }}
  - template: jinja
{% endfor %}
