My GNU/Linux & X environment
============================

![screenshot](http://bytebucket.org/sylvain_www/dotfiles/raw/dd105f16a362b88587a034df2f217fda4c459aec/screenshot.png)

# Components:
- [DWM][link_dwm] window manager (customised)
- notification area managed by python [STATNOT][link_sta] (customised)
- centralised command system in shell script .dwmcmd
- VIM bindings
- ZSH shell
- TMUX multiplexer
- integrated color system based on [SOLARIZED][link_sol]
- optimized for [TYPEMATRIX][link_typ] keyboard
- music system based on MPD

[link_dwm]: http://dwm.suckless.org
[link_sta]: http://github.com/halhen/statnot
[link_sol]: http://ethanschoonover.com/solarized
[link_typ]: http://www.typematrix.com

