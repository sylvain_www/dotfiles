# Installation instructions

### Backup files
```bash
# backup to 2x external drives
$ backup
# optional: backup firefox config for easier migration:
$ tar vfcz ~/backup/mozilla.tar.gz ~/.mozilla
# optional: backup auth cookies for easier migration:
$ tar vfcz ~/backup/contacts.tar.gz ~/.contacts
$ tar vfcz ~/backup/photos.tar.gz ~/.photos
```

## Install Ubuntu 22.04 server edition
* disk: full
* disk: no LVM
* user: temp
* no addtional packages

## Create final user with encrypted home
```bash
$ sudo apt install ecryptfs-utils
$ sudo adduser --home /home/user --encrypt-home user
$ sudo usermod -a -G sudo user
$ sudo deluser temp
$ sudo vim /etc/sudoers:
  + Defaults      env_keep += "HOME"
```

## Install dotfiles
```bash
$ git clone https://bitbucket.org/sylvain_www/dotfiles.git
$ echo <dotfiles_pass> > ~/.dotfiles.password
$ echo <database_pass> > ~/.database.password
$ ./apply
```

## Manual steps

### Restore files
```bash
$ backup --restore
```

### Firefox
Restore ~/.mozilla from backup, or follow instructions in firefox/init.sls.

### Dropbox
When prompted, logging to service in web browser.

### Contacts, Photos
The first time contacts/photos is called, complete the oauth
authentication flow.

In case of difficulties:
solution 1: override OOB_CALLBACK_URN in
/usr/lib/python3/dist-packages/oauth2client/tools.py
(see online directives)

solution 2: implement 'flags' in call to run_flow
as specified in /usr/lib/python3/dist-packages/oauth2client/tools.py

### Calendar
Run check_calendar manually and follow authentication procedure.

### Android mirror
On phone side:
```bash
# fixed IP 192.168.0.12
# developer option: wifi debug ON
```

On server side:
```bash
# update ADB platform tools >30
# https://developer.android.com/tools/releases/platform-tools
$ sudo rm /usr/lib/android-sdk/platform-tools
$ unzip platform-tools*.zip
$ sudo mv platform-tools /usr/lib/android-sdk/platform-tools
```

Pair procedure:
```bash
# on phone, access developer options for wifi debug
# start a pairing procedure on phone, then on server:
$ adb pair 192.168.0.12:$(nmap -sT 192.168.0.12 -p30000-49999 | awk -F/ '/tcp open/{print $1}')
```

### Photoprism display People and Calendar
Patch the server code:
```
$ docker build -f ~photos/samples/photoprism/dockerfile_patch . --tag photoprism:patched
```
and update ~/.photos/docker-compose.yml to use the new image

### Set GTK appearance
```bash
$ lxappearance
```
